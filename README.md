# Crossroads Carnival Roll20

Custom Roll20 Character Sheet, for use with Crossroads Carnival, published by Magpie Games

Crossroads Carnival is copyright Magpie Games

Includes:
* Basic Moves, with corresponding roll buttons
* Roll buttons for each stat
* repeating sections for adding moves, and character creation questions
* Fancy hexagons, like on the original character sheet
* Dynamic visual trackers, for XP, Darkness, and Harm
* Dynamic playbook banner

ToDo:
* add roll buttons for character moves
* Customize chat output for rolls
* Implement GM sheet
* Implement Apocalypse tracker
* Customize fonts / visuals
* add fields for hold, and +1 forward